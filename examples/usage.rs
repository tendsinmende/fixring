/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.*/

use fixring::RingBuffer;

fn main() {
    let mut buffer: RingBuffer<usize, 32> = RingBuffer::new();
    //pushing elements
    for i in 0..15 {
        buffer.push(i);
    }

    //popping elements back
    for _ in 0..6 {
        buffer.pop_back();
    }

    //iterate over current buffer
    for i in buffer.iter() {
        println!("element: {}", i);
    }

    //modify current buffer
    for i in buffer.iter_mut() {
        *i += 40;
    }

    //Checking modifies values
    for i in buffer.iter() {
        println!("element: {}", i);
    }
}
