# fixring

Fixed size Ringbuffer in Rust. Supports immutable and mutable iteration.

## Usage
Fixring tries to copy usage of common std structures like `Vec` or `HashMap`. Therefore `IntoIter` is implemented for `RingBuffer<T, N>`, `&RingBuffer<T, N>` and `&mut RingBuffer<T, N>`.
Have a look at the documentation to learn more.

### Usage example
```rust
use fixring::RingBuffer;

fn main(){
    let mut buffer: RingBuffer<usize, 32> = RingBuffer::new();
    //pushing elements
    for i in 0..15{
        buffer.push(i);
    }

    //popping elements back
    for _ in 0..6{
        buffer.pop_back();
    }

    //iterate over current buffer
    for i in buffer.iter(){
        println!("element: {}", i);
    }

    //modify current buffer
    for i in buffer.iter_mut(){
        *i += 40;
    }

    //Checking modifies values
    for i in buffer.iter(){
        println!("element: {}", i);
    }
}
```


## License
The whole project is licensed under MPL v2.0, all contributions will be licensed the same.
