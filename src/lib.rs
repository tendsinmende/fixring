/*This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.*/
#![no_std]
#![feature(maybe_uninit_uninit_array)]
use core::mem::MaybeUninit;

/// Allows you to crate fixed size ring buffers that can be iterated through, consumed and manipulated.
#[derive(Debug)]
pub struct RingBuffer<T, const SIZE: usize> {
    //We track initialization state via front and back pointers.
    buffer: [MaybeUninit<T>; SIZE],
    //front pointer, moving forward when pushing. Points to the next "to be written" slot.
    front: usize,
    //back pointer, moving forward when popping back. Points to the "to be poped" slot.
    back: usize,
    //tracks how many elements are in the buffer. Needed to distinguish the case where back == front. This can happen
    //if the buffer is empty, or full.
    num_elements: usize,
}

impl<T, const SIZE: usize> Clone for RingBuffer<T, SIZE>
where
    T: Copy,
{
    fn clone(&self) -> Self {
        RingBuffer {
            buffer: self.buffer.clone(),
            front: self.front,
            back: self.back,
            num_elements: self.num_elements,
        }
    }
}

impl<T, const SIZE: usize> RingBuffer<T, SIZE> {
    pub fn new() -> Self {
        RingBuffer {
            buffer: MaybeUninit::uninit_array(),
            back: 0,
            front: 0,
            num_elements: 0,
        }
    }

    ///Creates a new RingBuffer from an already initialized array. Therefore the resulting
    ///RingBuffer is already filled
    pub fn new_init(init: [T; SIZE]) -> Self {
        let mut buffer: [MaybeUninit<T>; SIZE] = MaybeUninit::uninit_array();
        for (idx, t) in IntoIterator::into_iter(init).enumerate() {
            buffer[idx] = MaybeUninit::new(t);
        }

        RingBuffer {
            buffer,
            front: 0,
            back: 0,
            num_elements: SIZE,
        }
    }

    ///Returns the number of elements the buffer can hold at max.
    pub fn capacity(&self) -> usize {
        SIZE
    }

    ///Pushes the element to the front of the RingBuffer. Might return the last element of the buffer, if the
    ///buffer is full.
    ///
    ///# Example
    ///```rust
    ///use fixring::RingBuffer;
    ///let mut buffer: RingBuffer<usize, 3> = RingBuffer::new();
    ///buffer.push(0).is_none();
    ///buffer.push(1).is_none();
    ///buffer.push(2).is_none();
    ///
    ///assert!(buffer.push(3).unwrap() == 0);
    ///```
    pub fn push(&mut self, element: T) -> Option<T> {
        if (self.front == self.back) && self.num_elements > 0 {
            //Overwriting back, therefore take back and return it, also move back pointer
            let mut new = MaybeUninit::new(element);
            //switch the old and the new element
            core::mem::swap(&mut new, &mut self.buffer[self.back]);

            //move back pointer
            self.back = (self.back + 1) % SIZE;
            //move front ptr as well
            self.front = (self.front + 1) % SIZE;
            //NOTE num elements doesnt change

            //unwrap overwritten element and return it.
            Some(unsafe { new.assume_init() })
        } else {
            //Not overwriting back, therefore the slot must be unused
            self.buffer[self.front] = MaybeUninit::new(element);
            self.front = (self.front + 1) % SIZE;
            self.num_elements += 1;
            None
        }
    }

    ///Tries to pop an element from the back. Returns `None` if there are no elements in the buffer.
    pub fn pop_back(&mut self) -> Option<T> {
        if self.len() == 0 {
            return None;
        }

        //is not empty, therefore the last element must be valid, remove it and move the back ptr
        let mut element = MaybeUninit::uninit();
        core::mem::swap(&mut element, &mut self.buffer[self.back]);

        //move back ptr
        self.back = (self.back + 1) % SIZE;
        //remove element from counter
        self.num_elements -= 1;

        return Some(unsafe { element.assume_init() });
    }

    ///Returns the element at `idx` counted from the back of the ringbuffer. Returns none if `idx > self.len()`.
    pub fn get(&self, idx: usize) -> Option<&T> {
        if idx >= self.len() {
            return None;
        }

        //is within range, therfore assume the T is init
        Some(unsafe { self.buffer[(self.back + idx) % SIZE].assume_init_ref() })
    }

    pub fn get_mut(&mut self, idx: usize) -> Option<&mut T> {
        if idx >= self.len() {
            return None;
        }

        //is within range, therfore assume the T is init
        Some(unsafe { self.buffer[(self.back + idx) % SIZE].assume_init_mut() })
    }

    pub fn len(&self) -> usize {
        self.num_elements
    }

    pub fn iter<'a>(&'a self) -> RingIter<'a, T, SIZE> {
        IntoIterator::into_iter(self)
    }

    pub fn iter_mut<'a>(&'a mut self) -> RingMutIter<'a, T, SIZE> {
        IntoIterator::into_iter(self)
    }
}

impl<T, const SIZE: usize> Drop for RingBuffer<T, SIZE> {
    fn drop(&mut self) {
        //clear buffer by taking all elements
        while let Some(e) = self.pop_back() {
            drop(e)
        }
    }
}

impl<T, const N: usize> IntoIterator for RingBuffer<T, N> {
    type Item = T;
    type IntoIter = RingIntoIter<T, N>;
    fn into_iter(self) -> Self::IntoIter {
        RingIntoIter(self)
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a RingBuffer<T, N> {
    type Item = &'a T;
    type IntoIter = RingIter<'a, T, N>;
    fn into_iter(self) -> Self::IntoIter {
        RingIter {
            buffer: self,
            idx: 0,
        }
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a mut RingBuffer<T, N> {
    type Item = &'a mut T;
    type IntoIter = RingMutIter<'a, T, N>;
    fn into_iter(self) -> Self::IntoIter {
        RingMutIter {
            buffer: self,
            idx: 0,
        }
    }
}

//Into Iterator
pub struct RingIntoIter<T, const N: usize>(RingBuffer<T, N>);
impl<T, const N: usize> Iterator for RingIntoIter<T, N> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        //The iterator advances until there is nothing left, this is the same as calling pop until nothing is
        //left
        self.0.pop_back()
    }
}

//Reference iterator
pub struct RingIter<'a, T, const N: usize> {
    buffer: &'a RingBuffer<T, N>,
    //Since we can assume that the buffer does not change in between calls, we can just start at
    //back and move forward until we reach front
    idx: usize,
}
impl<'a, T, const N: usize> Iterator for RingIter<'a, T, N> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        let element = self.buffer.get(self.idx);
        //Only count if there was one, prevents overflow when polling on Nons in this iter
        if element.is_some() {
            self.idx += 1;
        }
        element
    }
}

//Mut Reference iterator
pub struct RingMutIter<'a, T, const N: usize> {
    buffer: &'a mut RingBuffer<T, N>,
    //Since we can assume that the buffer does not change in between calls, we can just start at
    //back and move forward until we reach front
    idx: usize,
}
impl<'a, T, const N: usize> Iterator for RingMutIter<'a, T, N> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        let element = self.buffer.get_mut(self.idx);
        //Only count if there was one, prevents overflow when polling on Nons in this iter
        if let Some(element) = element {
            self.idx += 1;
            //Safety: Since the whole buffer must be borrowed for '_ the returned
            //element "blocks" the borrow checker from borrowing a second time.
            unsafe {
                let e: *mut T = element;
                let el: &'a mut T = &mut *e;
                Some(el)
            }
        } else {
            return None;
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::RingBuffer;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    //TODO test push overrun
    #[test]
    fn overwrite() {
        let mut buffer: RingBuffer<usize, 3> = RingBuffer::new();
        assert!(buffer.push(0).is_none());
        assert!(buffer.push(1).is_none());
        assert!(buffer.push(2).is_none());
        //Trigger overrun some times
        for i in 0..25 {
            assert!(buffer.push(i % 3).unwrap() == i % 3);
        }
    }

    #[test]
    fn remove_easy() {
        let mut buffer: RingBuffer<usize, 32> = RingBuffer::new();
        for i in 0..32 {
            assert!(buffer.push(i).is_none());
        }

        for i in 0..32 {
            assert!(buffer.pop_back().unwrap() == i, "wasnt {}", i);
        }

        assert!(buffer.len() == 0);
    }

    #[test]
    fn remove_wrapped() {
        let mut buffer: RingBuffer<usize, 32> = RingBuffer::new();
        for i in 0..(32 * 2) {
            buffer.push(i % 32);
        }

        for i in 0..32 {
            assert!(buffer.pop_back().unwrap() == i);
        }

        assert!(buffer.len() == 0);
    }

    #[test]
    fn init_array() {
        let mut buf = RingBuffer::new_init([0, 1, 2, 3, 4, 5, 6, 7]);

        //Check all elements
        for i in 0..8 {
            assert!(buf.push(i).unwrap() == i);
        }

        assert!(buf.pop_back().unwrap() == 0);
        assert!(buf.pop_back().unwrap() == 1);
        assert!(buf.pop_back().unwrap() == 2);
        assert!(buf.pop_back().unwrap() == 3);

        assert!(buf.get(0).unwrap() == &4);
        assert!(buf.get(1).unwrap() == &5);
        assert!(buf.get(2).unwrap() == &6);
        assert!(buf.get(3).unwrap() == &7);

        *buf.get_mut(3).unwrap() = 42;
        assert!(buf.get(3).unwrap() == &42);
    }

    #[test]
    fn clone() {
        let mut buf = RingBuffer::new_init([0, 1, 2, 3, 4, 5, 6, 7]);

        //Check all elements
        for i in 0..8 {
            assert!(buf.push(i).unwrap() == i);
        }

        let mut cloned = buf.clone();

        //Check all elements
        for i in 0..8 {
            assert!(cloned.push(i).unwrap() == i);
        }

        assert!(buf.pop_back().unwrap() == 0);
        assert!(buf.pop_back().unwrap() == 1);
        assert!(buf.pop_back().unwrap() == 2);
        assert!(buf.pop_back().unwrap() == 3);

        assert!(buf.get(0).unwrap() == &4);
        assert!(buf.get(1).unwrap() == &5);
        assert!(buf.get(2).unwrap() == &6);
        assert!(buf.get(3).unwrap() == &7);
        *buf.get_mut(3).unwrap() = 42;
        assert!(buf.get(3).unwrap() == &42);

        assert!(cloned.pop_back().unwrap() == 0);
        assert!(cloned.pop_back().unwrap() == 1);
        assert!(cloned.pop_back().unwrap() == 2);
        assert!(cloned.pop_back().unwrap() == 3);

        assert!(cloned.get(0).unwrap() == &4);
        assert!(cloned.get(1).unwrap() == &5);
        assert!(cloned.get(2).unwrap() == &6);
        assert!(cloned.get(3).unwrap() == &7);
        *cloned.get_mut(3).unwrap() = 42;
        assert!(cloned.get(3).unwrap() == &42);
    }
}
